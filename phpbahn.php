<?php
class phpbahn {

	private $apikey;
	private $apis;

	function __construct($apikey){
		$this->apikey = $apikey;

		if(strlen($apikey) < 5){
			trigger_error("Fehler: Kein API-Key angegeben.", 256);
			return false;
		}

		$this->apis = array("timetables" => array("url"=> "https://api.deutschebahn.com/timetables/v1/", "return" => "xml" ), "fahrplan-plus"=> array("url"=> "https://api.deutschebahn.com/fahrplan-plus/v1/", "return"=>"json" ));

		$this->debug_mode = true;

		$this->causes_list = array( "0" =>"keine Verspätungsbegründung",
		"2" =>"Polizeiliche Ermittlung",
		"3" =>"Feuerwehreinsatz an der Strecke",
		"4" =>"kurzfristiger Personalausfall",
		"5" =>"ärztliche Versorgung eines Fahrgastes",
		"6" =>"Betätigen der Notbremse",
		"7" =>"Personen im Gleis",
		"8" =>"Notarzteinsatz am Gleis",
		"9" =>"Streikauswirkungen",
		"10" =>"Tiere im Gleis",
		"11" =>"Unwetter",
		"12" =>"Warten auf ein verspätetes Schiff",
		"13" =>"Pass- und Zollkontrolle",
		"14" =>"Technische Störung am Bahnhof",
		"15" =>"Beeinträchtigung durch Vandalismus",
		"16" =>"Entschärfung einer Fliegerbombe",
		"17" =>"Beschädigung einer Brücke",
		"18" =>"umgestürzter Baum im Gleis",
		"19" =>"Unfall an einem Bahnübergang",
		"20" =>"Tiere im Gleis",
		"21" =>"Warten auf Fahrgäste aus einem anderen Zug",
		"22" =>"Witterungsbedingte Störung",
		"23" =>"Feuerwehreinsatz auf Bahngelände",
		"24" =>"Verspätung im Ausland",
		"25" =>"Warten auf weitere Wagen",
		"28" =>"Gegenstände im Gleis",
		"29" =>"Ersatzverkehr mit Bus ist eingerichtet",
		"31" =>"Bauarbeiten",
		"32" =>"Verzögerung beim Ein-/Ausstieg",
		"33" =>"Oberleitungsstörung",
		"34" =>"Signalstörung",
		"35" =>"Streckensperrung",
		"36" =>"technische Störung am Zug",
		"38" =>"technische Störung an der Strecke",
		"39" =>"Anhängen von zusätzlichen Wagen",
		"40" =>"Stellwerksstörung /-ausfall",
		"41" =>"Störung an einem Bahnübergang",
		"42" =>"außerplanmäßige Geschwindigkeitsbeschränkung",
		"43" =>"Verspätung eines vorausfahrenden Zuges",
		"44" =>"Warten auf einen entgegenkommenden Zug",
		"45" =>"Überholung",
		"46" =>"Warten auf freie Einfahrt",
		"47" =>"verspätete Bereitstellung des Zuges",
		"48" =>"Verspätung aus vorheriger Fahrt",
		"55" =>"technische Störung an einem anderen Zug",
		"56" =>"Warten auf Fahrgäste aus einem Bus",
		"57" =>"Zusätzlicher Halt zum Ein-/Ausstieg für Reisende",
		"58" =>"Umleitung des Zuges",
		"59" =>"Schnee und Eis",
		"60" =>"Reduzierte Geschwindigkeit wegen Sturm",
		"61" =>"Türstörung",
		"62" =>"behobene technische Störung am Zug",
		"63" =>"technische Untersuchung am Zug",
		"64" =>"Weichenstörung",
		"65" =>"Erdrutsch",
		"66" =>"Hochwasser",
		"99" =>"Verzögerungen im Betriebsablauf");

		$this->quality_list = array("70" =>"WLAN im gesamten Zug nicht verfügbar",
		"71" =>"WLAN in einem/mehreren Wagen nicht verfügbar",
		"72" =>"Info-/Entertainment nicht verfügbar",
		"73" =>"Heute: Mehrzweckabteil vorne",
		"74" =>"Heute: Mehrzweckabteil hinten",
		"75" =>"Heute: 1. Klasse vorne",
		"76" =>"Heute: 1. Klasse hinten",
		"77" =>"ohne 1. Klasse",
		"79" =>"ohne Mehrzweckabteil",
		"80" =>"andere Reihenfolge der Wagen",
		"82" =>"mehrere Wagen fehlen",
		"83" =>"Störung fahrzeuggebundene Einstiegshilfe",
		"84" =>"Zug verkehrt richtig gereiht",
		"85" =>"ein Wagen fehlt",
		"86" =>"gesamter Zug ohne Reservierung",
		"87" =>"einzelne Wagen ohne Reservierung",
		"88" =>"keine Qualitätsmängel",
		"89" =>"Reservierungen sind wieder vorhanden",
		"90" =>"kein gastronomisches Angebot",
		"91" =>"fehlende Fahrradbeförderung",
		"92" =>"Eingeschränkte Fahrradbeförderung",
		"93" =>"keine behindertengerechte Einrichtung",
		"94" =>"Ersatzbewirtschaftung",
		"95" =>"Ohne behindertengerechtes WC",
		"96" =>"Überbesetzung mit Kulanzleistungen",
		"97" =>"Überbesetzung ohne Kulanzleistungen",
		"98" =>"sonstige Qualitätsmängel");
	}

	private function bahnCurl($request, $api ){
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $this->apis[$api]['url'].$request);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");

		//echo $this->apis[$api]['url'].$request;

		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

		$headers = array();

		$headers[] = "Authorization: Bearer ".$this->apikey;
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

		$result = curl_exec($ch);

		if (curl_errno($ch)) {
			trigger_error('Fehler:' . curl_error($ch));
			return false;
		}
		curl_close ($ch);

		if($this->apis[$api]['return'] == "xml" ){
			$xml = simplexml_load_string($result, "SimpleXMLElement", LIBXML_NOCDATA);
			$json = json_encode($xml);
			$array = json_decode($json,TRUE);
		}elseif($this->apis[$api]['return'] == "json" ){
			$array = json_decode($result,TRUE);

		}


		return $array;


	}

	public function getTimetable($stationID, $time = 0){

		if($time == 0){
			$time = time();
		}

		$date = date("ymd", $time);
		$hour = date ("H", $time);

		$requestAbweichung = "fchg/".$stationID;
		//echo "-------------------" . $stationID;
		$abweichungen = $this->bahnCurl($requestAbweichung, "timetables" );

		$sortAbweichung = array();

		//print_r($abweichungen);
		foreach($abweichungen['s'] as $abw){
			$sortAbweichung[$abw['@attributes']['id']] = $abw;
		}
		//print_r($sortAbweichung);


		$requestFahrten = 'plan/'.$stationID.'/'.$date.'/'.$hour;
		//echo "REQ: " . $requestFahrten;
		$fahrten = $this->bahnCurl($requestFahrten, "timetables");

		$sortFahrten = array();
		foreach($fahrten['s'] as $fahrt){
			$id = $fahrt['@attributes']['id'];
			unset($fahrt['@attributes']);

			$fahrt['zug']['typ'] = $fahrt['tl']['@attributes']['t'];
			$fahrt['zug']['owner'] = $fahrt['tl']['@attributes']['o'];
			$fahrt['zug']['klasse'] = $fahrt['tl']['@attributes']['c'];
			$fahrt['zug']['nummer'] = $fahrt['tl']['@attributes']['n'];

			unset($fahrt['tl']);

			$cancelstates = array("a"=>"added", "c"=>"cancelled", "p"=>"planned");

			if(isset($fahrt['ar'])){

				if(isset($fahrt['ar']['@attributes']['l'])){
					$fahrt['ankunft']['line'] = $fahrt['ar']['@attributes']['l'];
				}

				$fahrt['ankunft']['zeitGeplant'] = $fahrt['ar']['@attributes']['pt'];
				if(isset($sortAbweichung[$id]['ar']['@attributes']['ct'])){
					$fahrt['ankunft']['zeitAktuell'] = $sortAbweichung[$id]['ar']['@attributes']['ct'];

					//OWN STUFF
					if(isset($sortAbweichung[$id]['ar']['m'])){
						$causes = array();
						$quality = array();
						foreach($sortAbweichung[$id]['ar']['m'] as $msg){
							if(isset($msg['@attributes']['t'])){
								if($msg['@attributes']['t'] == "d"){
									//echo $msg['@attributes']['c'] . "######";
									if(!in_array($msg['@attributes']['c'], $causes)){
										array_push($causes, $msg['@attributes']['c']);
									}
								}else if($msg['@attributes']['t'] == "q"){
									//echo $msg['@attributes']['c'] . "######";
									if(!in_array($msg['@attributes']['c'], $quality)){
										array_push($quality, $msg['@attributes']['c']);
									}
								}
							}
						}
						//var_dump($causes);
						if(sizeof($causes) > 0){
							$fahrt['ankunft']['verspaetungsGruende'] = $causes;
							if(isset($this->causes_list[$causes[sizeof($causes) - 1]])) {
								$fahrt['ankunft']['verspaetungsGruendeText'] = $this->causes_list[$causes[sizeof($causes) - 1]];
								if($this->debug_mode && !isset($this->causes_list[$causes[sizeof($causes) - 1]])){
									$fahrt['ankunft']['verspaetungsGruendeText'] = $causes[sizeof($causes) - 1];
								}
							} else {
								$fahrt['ankunft']['verspaetungsGruendeText'] =  " CODE: " . $causes[sizeof($causes) - 1];
							}
							//echo $fahrt['ankunft']['verspaetungsGruendeText'];
						}

						if(sizeof($quality) > 0){
							$fahrt['ankunft']['qualitaetsMaengel'] = $quality;
							$fahrt['ankunft']['qualitaetsMaengelText'] = "";
							foreach($quality as $quali){
								$fahrt['ankunft']['qualitaetsMaengelText'] .= $this->quality_list[$quali] . ", ";
							}
						}
					}
				}

				$fahrt['ankunft']['gleisGeplant'] = $fahrt['ar']['@attributes']['pp'];
				if(isset($sortAbweichung[$id]['ar']['@attributes']['cp'])){
					$fahrt['ankunft']['gleisAktuell'] = $sortAbweichung[$id]['ar']['@attributes']['cp'];
				}

				$fahrt['ankunft']['routeGeplant'] = explode("|",$fahrt['ar']['@attributes']['ppth']);
				if(isset($sortAbweichung[$id]['ar']['@attributes']['cpth'])){
					$fahrt['ankunft']['routeAktuell'] = explode("|",$sortAbweichung[$id]['ar']['@attributes']['cpth']);
				}

				if(isset($sortAbweichung[$id]['ar']['@attributes']['cs'])){
					$fahrt['ankunft']['cancel'] = $cancelstates[$sortAbweichung[$id]['ar']['@attributes']['cs']];
				}

			}


			if(isset($fahrt['dp'])){

				if(isset($fahrt['dp']['@attributes']['l'])){
					$fahrt['abfahrt']['line'] = $fahrt['dp']['@attributes']['l'];
				}

				$fahrt['abfahrt']['zeitGeplant'] = $fahrt['dp']['@attributes']['pt'];
				if(isset($sortAbweichung[$id]['dp']['@attributes']['ct'])){
					$fahrt['abfahrt']['zeitAktuell'] = $sortAbweichung[$id]['dp']['@attributes']['ct'];
				}

				$fahrt['abfahrt']['gleisGeplant'] = $fahrt['dp']['@attributes']['pp'];
				if(isset($sortAbweichung[$id]['dp']['@attributes']['cp'])){
					$fahrt['abfahrt']['gleisAktuell'] = $sortAbweichung[$id]['dp']['@attributes']['cp'];
				}

				$fahrt['abfahrt']['routeGeplant'] = explode("|",$fahrt['dp']['@attributes']['ppth']);
				if(isset($sortAbweichung[$id]['dp']['@attributes']['cpth'])){
					$fahrt['abfahrt']['routeAktuell'] = explode("|",$sortAbweichung[$id]['dp']['@attributes']['cpth']);
				}


				if(isset($sortAbweichung[$id]['dp']['@attributes']['cs'])){
					$fahrt['abfahrt']['cancel'] = $cancelstates[$sortAbweichung[$id]['dp']['@attributes']['cs']];
				}

			}

			unset($fahrt['ar']);
			unset($fahrt['dp']);

			$sortFahrten[$id] = $fahrt;

		}

		return $sortFahrten;


	}
	
	public function dateToTimestamp($bahndatum){
		$date = DateTime::createFromFormat('ymdHi', $bahndatum);
		return date_timestamp_get($date);
	}
}

?>
