# CommandLine DB Departures

TODO

## Get DB API Key:

1. Create Development Account: https://developer.deutschebahn.com/store/site/pages/sign-up.jag
2. Log in to your Account
3. Create a new "Anwendung" in the Menu "Meine Anwendungen"
4. Subscribe to the following API(s): Timetables
5. Change to tab "Meine Abonnements"
6. Create a new production api key
7. Copy the API key to the settings.php (DON'T push this file!)
