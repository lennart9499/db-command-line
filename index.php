<?php
include('phpbahn.php');
include('settings.php');

$bahn = new phpbahn(SETTING_APIKEY);

//----------------


function getJSONStation($search){
	$ch = curl_init();

	curl_setopt($ch, CURLOPT_URL, 'https://reiseauskunft.bahn.de/bin/ajax-getstop.exe/dn?encoding=utf-8&L=vs_test_fsugg_getstop&start=1&tpl=sls&REQ0JourneyStopsB=12&REQ0JourneyStopsS0A=7&getstop=1&noSession=yes&iER=yes&S=' . urlencode($search) . '?&REQ0JourneyStopsS0a=131072&REQ0JourneyStopsS0o=8&js=true&');
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');

	curl_setopt($ch, CURLOPT_ENCODING, 'gzip, deflate');

	$headers = array();
	$headers[] = 'Sec-Fetch-Mode: no-cors';
	$headers[] = 'Referer: https://www.bahn.de/p/view/index.shtml';
	$headers[] = 'User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/77.0.3865.90 Safari/537.36';
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

	$result = curl_exec($ch);
	if (curl_errno($ch)) {
		echo 'Error:' . curl_error($ch);
	}
	curl_close($ch);

	return json_decode(substr(substr_replace($result ,"",-22), 8));
}

if(!isset($_GET['s'])){
	echo "Wrong Usage: curl -L lennartkaiser.de/cb/?s=NAME\n\n";
	die();
}

$suggestions = getJSONStation($_GET['s']);
/*
foreach ($suggestions->suggestions[0]->extId as $value){
	echo $value->extId . " " . $value->value . "<br>";
}
*/

//----------------
$ibnr = $suggestions->suggestions[0]->extId;
$bhf['name'] = $suggestions->suggestions[0]->value . " [" . $suggestions->suggestions[0]->extId . "]";
//----------------
function dateToTimestamp($bahndatum){
	$date = DateTime::createFromFormat('ymdHi', $bahndatum);
	return date_timestamp_get($date);
}

$output .= "\n\n";
$output .= str_repeat('─', 120) . "\n" . $bhf['name'] . "\n" . str_repeat('─', 120);
$output .= "\nZug              Ziel                  Abfahrt   Gleis  Über                                                   Anmerkungen\n";

for($v = 0; $v < 2; $v++){
	$zuege = $bahn->getTimetable($ibnr, strtotime($v . ' hour'));

	//SORTING FOR CORRECT ORDER:
	foreach($zuege as $key => $value) {
		//still going to sort by firstname
		if(isset($value['abfahrt']['zeitGeplant'])){
			$emp[$key] = date("H:i", dateToTimestamp($value['abfahrt']['zeitGeplant']));
		}else{
			$emp[$key] = "00:00";
		}
	}
	array_multisort($emp, SORT_ASC, $zuege);



	foreach($zuege as $zug){
		//only departures
		if(isset($zug['abfahrt'])){

			//ZIEL
			$ziel = array_pop($zug['abfahrt']['routeGeplant']) . "      ";

			//STRECKE
			$strecke = implode(", ", array_slice($zug['abfahrt']['routeGeplant'], 0, 3));

			//BEMERKUNGEN
			$verspGrund = "";
			if(isset($zug['ankunft']['verspaetungsGruendeText'])){
				$verspGrund = $zug['ankunft']['verspaetungsGruendeText'];
			}
			$qualiGrund = "";
			if(isset($zug['ankunft']['qualitaetsMaengelText'])){
				$qualiGrund = $zug['ankunft']['qualitaetsMaengelText'];
			}

			//TRAIN TYPE + NO + DISPLAY NO
			$train_type = $zug['zug']['klasse']." ".$zug['zug']['nummer'];
			if(isset($zug['abfahrt']['line'])){
				$short = $zug['abfahrt']['line'];
				if(mb_strlen(trim($zug['abfahrt']['line']), "utf-8") <= 2){
					$short = substr($train_type, 0, 2) . $short;
				}
				$train_type = trim($train_type) . str_repeat(' ', max(0, 9 - mb_strlen(trim($train_type)))) . " | " .  $short;
			}

			//Dep Time / cancelled
			if($zug['abfahrt']['cancel'] == "cancelled"){
				$dep_time = "FÄLLT AUS";
			}else{
				$diff = ($zug['abfahrt']['zeitAktuell'] - $zug['abfahrt']['zeitGeplant']);
				$dep_time = date("H:i", $bahn->dateToTimestamp($zug['abfahrt']['zeitGeplant']));
				if($diff < 10000 && $diff > -10000){
					$dep_time .= "+" . ($diff) . "   ";
				}
			}

			//Departure platform
			if(isset($zug['abfahrt']['gleisAktuell'])){
				$ou = $zug['abfahrt']['gleisAktuell'];
			}else{
				$ou = $zug['abfahrt']['gleisGeplant'];
			}

			//Printing each line

			if($verspGrund != "" && $qualiGrund != ""){
				$ver = "".$verspGrund. " + " . $qualiGrund;
			}else{
				$ver = "".$verspGrund. $qualiGrund;
			}

			$output .= trim($train_type) . str_repeat(' ', max(0, 17 - mb_strlen(trim($train_type), "utf-8")));
			$output .= trim($ziel) . str_repeat(' ', max(0, 22 - mb_strlen(trim($ziel), "utf-8")));
			$output .= trim($dep_time) . str_repeat(' ', max(0, 10 - mb_strlen(trim($dep_time), "utf-8")));
			$output .= trim($ou) . str_repeat(' ', max(0, 7 - mb_strlen(trim($ou), "utf-8")));
			$output .= trim($strecke) . str_repeat(' ', max(0, 55 - mb_strlen(trim($strecke), "utf-8")));
			$output .= trim($ver);


			$output .= "\n\n";
		}
	}
}
$output .= str_repeat('─', 120) . "\n\n" ;
//Ausgabe
//echo "\e[1;104m";
echo $output;
//echo "\e[0m\n";
//echo str_repeat(' ', 120);
?>
